package tradingview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.model.FavoriteIdea;
import tradingview.model.Like;

import java.util.List;
import java.util.Optional;

public interface FavoriteIdeaRepository extends JpaRepository<FavoriteIdea, Long> {

    @Query(value = "SELECT * FROM favorite_ideas fi" +
            " WHERE fi.user_id = :userId " +
            "AND fi.idea_id = :ideaId", nativeQuery = true)
    Optional<FavoriteIdea> findFavoriteIdeaByUserIdAndIdeaId(@Param("userId") Long userId, @Param("ideaId") Long ideaId);

    @Query(value = "SELECT * FROM favorite_ideas fi" +
            " WHERE fi.user_id = :userId ", nativeQuery = true)
    List<FavoriteIdea> findFavoriteIdeasByUserId(@Param("userId") Long userId);

    @Query(value = "SELECT COUNT(*) FROM favorite_ideas fi" +
            " WHERE fi.idea_id = :ideaId AND fi.user_id= :userId", nativeQuery = true)
    Integer countFavoriteIdeasByIdeaIdAndUserId(@Param("ideaId") Long ideaId, @Param("userId") Long userId);
}
