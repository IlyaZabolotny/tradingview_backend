package tradingview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.model.User;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users u WHERE u.login LIKE %:keyword%", nativeQuery = true)
    List<User> findAllUsers(@Param("keyword") String keyword);

    @Query(value = "SELECT * FROM users u WHERE u.login = :login", nativeQuery = true)
    Optional<User> findByLogin(String login);

    @Query(value = "SELECT * FROM users u where id in (select subscribed_to from subscriptions" +
            " WHERE user_id = :idUser AND u.login LIKE %:keyword%)", nativeQuery = true)
    List<User> findAllSubscriptions(@Param("idUser") long idUser, @Param("keyword") String keyword);

    @Query(value = "SELECT * FROM users u where id in (select user_id from subscriptions" +
            " WHERE subscribed_to = :idUser AND u.login LIKE %:keyword%)", nativeQuery = true)
    List<User> findAllSubscribers(@Param("idUser") long idUser, @Param("keyword") String keyword);

    @Query(value = "SELECT COUNT(user_id) FROM subscriptions s WHERE s.subscribed_to = :idUser", nativeQuery = true)
    Long countFollowers(Long idUser);

    @Query(value = "SELECT COALESCE(SUM(likes), 0) FROM ideas i WHERE i.author_id = :idUser", nativeQuery = true)
    Long getAllLikes(Long idUser);

    @Query(value = "SELECT COUNT(id) FROM ideas i WHERE i.author_id = :idUser", nativeQuery = true)
    Long countAllIdeas(Long idUser);
}