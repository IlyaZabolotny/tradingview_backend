package tradingview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.model.FavoriteStock;

import java.util.List;
import java.util.Optional;

public interface StockRepository extends JpaRepository<FavoriteStock,Long> {

    @Query(value = "SELECT * FROM favorite_stocks s" +
            " WHERE s.user_id = :userId " +
            "AND s.symbol = :symbol", nativeQuery = true)
    Optional<FavoriteStock> findStockByUserIdAndSymbol(@Param("userId") Long userId, @Param("symbol") String symbol);

    @Query(value = "SELECT * FROM favorite_stocks s" +
            " WHERE s.user_id = :userId ", nativeQuery = true)
    List<FavoriteStock> findStocksByUserId(@Param("userId") Long userId);
}
