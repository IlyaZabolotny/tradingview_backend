package tradingview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.model.Like;

import java.util.List;
import java.util.Optional;

public interface LikeRepository extends JpaRepository<Like, Long> {

    @Query(value = "SELECT * FROM likes l" +
            " WHERE l.user_id = :userId " +
            "AND l.idea_id = :ideaId", nativeQuery = true)
    Optional<Like> findLikeByUserIdAndIdeaId(@Param("userId") Long userId, @Param("ideaId") Long ideaId);

    @Query(value = "SELECT * FROM likes l" +
            " WHERE l.idea_id = :ideaId ", nativeQuery = true)
    List<Like> findLikesByIdeaId(@Param("ideaId") Long ideaId);

    @Query(value = "SELECT * FROM likes l" +
            " WHERE l.user_id = :userId ", nativeQuery = true)
    List<Like> findLikesByUserId(@Param("userId") Long userId);

    @Query(value = "SELECT COUNT(*) FROM likes l" +
            " WHERE l.idea_id = :ideaId ", nativeQuery = true)
    Integer countLikesByIdeaId(@Param("ideaId") Long ideaId);

    @Query(value = "SELECT COUNT(*) FROM likes l" +
            " WHERE l.idea_id = :ideaId AND l.user_id= :userId", nativeQuery = true)
    Integer countLikesByIdeaIdAndUserId(@Param("ideaId") Long ideaId, @Param("userId") Long userId);
}
