package tradingview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.model.Subscription;

import java.util.Optional;

public interface SubscriptionRepository extends JpaRepository<Subscription,Long> {

    @Query(value = "SELECT * FROM subscriptions s" +
            " WHERE s.user_id = :userId " +
            "AND s.subscribed_to = :subscribedTo", nativeQuery = true)
    Optional<Subscription> findSubscriptionByUserIdAndSubscriberId(@Param("userId") Long userId,
                                                                   @Param("subscribedTo") Long subscribedTo);

    @Query(value = "SELECT COUNT(*) FROM subscriptions s" +
            " WHERE s.user_id = :userId " +
            "AND s.subscribed_to = :subscribedTo", nativeQuery = true)
    Integer checkSubscriptionByUserIdAndSubscriberId(@Param("userId") Long userId,
                                                     @Param("subscribedTo") Long subscribedTo);
}
