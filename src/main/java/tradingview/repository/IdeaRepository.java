package tradingview.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.dto.IdeaListDTO;
import tradingview.dto.SearchDTO;
import tradingview.model.Idea;
import java.util.List;

public interface IdeaRepository extends JpaRepository<Idea,Long> {

//    @Query(value = "SELECT i " + "FROM  Idea i WHERE " +
//            "i.title LIKE %:#{#search.getTitle()}%")
//    List<Idea> findIdeasByTitle(@Param("search")SearchDTO searchDTO);

    @Query(value = "SELECT * FROM ideas i WHERE i.author_id = :authorId", nativeQuery = true)
    List<Idea> findIdeasByAuthor(@Param("authorId") long authorId);

//    @Query(value="SELECT * FROM ideas i WHERE i.id = :ideaId", nativeQuery = true)
//    List<Idea> findIdeaById(@Param("ideaId") long ideaId);

    @Query(value = "SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id WHERE i.id IN" +
            " (SELECT idea_id FROM favorite_ideas WHERE user_id = :authorId)" +
            " AND i.title LIKE %:keyword%", nativeQuery = true)
    List<IdeaListDTO> findFavoriteIdeas(@Param("authorId") long authorId,
                                        @Param("keyword") String keyword);

    @Query(value="SELECT * FROM ideas WHERE id IN (SELECT idea_id FROM idea_tag WHERE tag_id IN (" +
            "SELECT id FROM tags WHERE tag_name = :tag))", nativeQuery = true)
    List<Idea> findIdeasByTag(String tag);

    @Query(value="SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id" +
            " WHERE i.author_id = :authorId AND i.title LIKE %:keyword%", nativeQuery = true)
    List<IdeaListDTO> getIdeasListSearch(@Param("authorId") Long authorId,
                                         @Param("keyword") String keyword);

    @Query(value="SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id" +
            " WHERE i.author_id = :authorId", nativeQuery = true)
    List<IdeaListDTO> getIdeasList(@Param("authorId") Long authorId);

    @Query(value="SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id " +
            "WHERE i.title LIKE %:keyword%", nativeQuery = true)
    List<IdeaListDTO> findIdeasByTitle(@Param("keyword") String keyword);

    @Query(value="SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id " +
            "WHERE i.title LIKE %:keyword%", nativeQuery = true)
    Page<IdeaListDTO> findIdeasByTitleUnauthorized(@Param("keyword") String keyword, Pageable pageable);

    @Query(value="SELECT i.id, i.content, i.image, i.created, i.title, u.login, u.avatar" +
            "  FROM ideas i INNER JOIN users u ON u.id=i.author_id", nativeQuery = true)
    Page<IdeaListDTO> findIdeas(Pageable pageable);
}