package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import tradingview.dto.FullIdeaDTO;
import tradingview.dto.IdeaDTO;
import tradingview.dto.IdeaListDTO;
import tradingview.model.Idea;
import tradingview.model.User;
import tradingview.service.IdeaService;
import tradingview.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/ideas")
public class IdeaController {

    @Autowired
    private IdeaService ideaService;

    @Autowired
    private UserService userService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping
    public PagedListHolder<FullIdeaDTO> searchByTitle(@RequestParam("userId") Long userId,
                                                      @RequestParam("keyword") String keyword,
                                                      @RequestParam("pageNumber") int pageNumber,
                                                      @RequestParam("size") int size) {
        return ideaService.findIdeasByTitle(userId, keyword, pageNumber, size);
    }

    @GetMapping("/unauthorized")
    public Page<IdeaListDTO> searchByTitleUnauthorized(@RequestParam("keyword") String keyword,
                                                       @RequestParam("pageNumber") int pageNumber,
                                                       @RequestParam("size") int size) {
        return ideaService.findIdeasByTitleUnauthorized(keyword, pageNumber, size);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Idea> getIdeaById(@PathVariable long id) {
        Idea idea = ideaService.findIdeaById(id);
        return idea != null ? ResponseEntity.ok(idea) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addIdea(@RequestBody IdeaDTO ideaDTO) {
        Idea idea = new Idea();
        idea.setTitle(ideaDTO.getTitle());
        idea.setContent(ideaDTO.getContent());
        idea.setImage(ideaDTO.getImage());
        idea.setAuthor(ideaDTO.getAuthor());
        List<User> followers = userService.findAllSubscribers(idea.getAuthor(), "");
        for (User follower: followers) {
            simpMessagingTemplate.convertAndSend("/topic/notifications/" + follower.getId(),
                    userService.findUserById(ideaDTO.getAuthor()).getLogin() + " has published a new idea");
        }
        ideaService.addIdea(idea);
    }

    @PatchMapping("/update")
    public void updateIdea(@RequestBody IdeaDTO idea) {
        ideaService.patchIdea(idea);
    }

    @GetMapping("/author/list")
    public PagedListHolder<FullIdeaDTO> getIdeasList(@RequestParam("userId") long userId,
                                          @RequestParam("keyword") String keyword,
                                          @RequestParam("pageNumber") int pageNumber,
                                          @RequestParam("size") int size) {
        return ideaService.findPublishedIdeas(userId, keyword, pageNumber, size);
    }

    @GetMapping("/favorites")
    public PagedListHolder<FullIdeaDTO> getFavoriteIdeas(@RequestParam long id,
                                              @RequestParam("keyword") String keyword,
                                              @RequestParam("pageNumber") int pageNumber,
                                              @RequestParam("size") int size) {
        return ideaService.findFavoriteIdeas(id, keyword, pageNumber, size);
    }

    @GetMapping("/tag")
    public List<Idea> searchByTag(@RequestParam String tag) {
        return ideaService.findIdeasByTag(tag);
    }

}