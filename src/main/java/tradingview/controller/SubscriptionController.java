package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tradingview.model.Like;
import tradingview.model.Subscription;
import tradingview.service.LikeService;
import tradingview.service.SubscriptionService;

@RestController
@RequestMapping("/subscriptions")
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping("/add")
    public void setSubscription(@RequestBody Subscription subscription) {
        subscriptionService.addSubscription(subscription);
    }

    @DeleteMapping("/delete")
    public void deleteSubscription(@RequestBody Subscription subscription) {
        subscriptionService.deleteSubscription(subscription);
    }
}
