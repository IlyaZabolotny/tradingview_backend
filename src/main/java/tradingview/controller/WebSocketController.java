package tradingview.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @MessageMapping("/websocket")
    @SendTo("/topic/notifications/{id}")
    public String sendNotification(@DestinationVariable String id) throws Exception {
        return "ok";
    }
}
