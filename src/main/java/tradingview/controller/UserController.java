package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tradingview.dto.*;
import tradingview.model.User;
import tradingview.service.LikeService;
import tradingview.service.UserService;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private LikeService likeService;

    @GetMapping("/unauthorized")
    public PagedListHolder<FollowerDTO> getUsersUnauthorized(@RequestParam("keyword") String keyword,
                                                             @RequestParam("pageNumber") int pageNumber,
                                                             @RequestParam("size") int size) {
        List<User> users = userService.findAllUsers(keyword);
        PagedListHolder<FollowerDTO> result = userService.getAdditionalInfo(users, pageNumber, size);
        return result;
    }

    @GetMapping
    public PagedListHolder<UserListDTO> getUsers(@RequestParam("userId") Long userId,
                                                 @RequestParam("keyword") String keyword,
                                                 @RequestParam("pageNumber") int pageNumber,
                                                 @RequestParam("size") int size) {
        List<User> users = userService.findAllUsers(keyword);
        PagedListHolder<UserListDTO> result = userService.getAdditionalInfoAuthorized(users, userId, pageNumber, size);
        return result;
    }

    @GetMapping("/{id}")
    public ResponseEntity<HeaderDTO> getUserById(@PathVariable("id") Long id) {
        User user = userService.findUserById(id); //Long.valueOf(id));
        HeaderDTO headerDTO = new HeaderDTO();
        headerDTO.setAvatar(user.getAvatar());
        headerDTO.setLogin(user.getLogin());
        return ResponseEntity.ok(headerDTO);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }

    @PatchMapping("/update")
    public void updateUser(@RequestBody UserSettingsDTO userSettingsDTO) {
        User user = userService.findUserById(userSettingsDTO.getId());
        user.setEmail(userSettingsDTO.getEmail());
        user.setPassword(userSettingsDTO.getPassword());
        user.setFirstName(userSettingsDTO.getFirstName());
        user.setLastName(userSettingsDTO.getLastName());
        user.setCompany(userSettingsDTO.getCompany());
        if (userSettingsDTO.getAvatar() != null) {
            user.setAvatar(userSettingsDTO.getAvatar());
        }
        userService.patchPasswordUser(user);
    }

    @GetMapping("/profile/{id}")
    public ResponseEntity<ProfileDTO> getUserProfile(@PathVariable Long id) {
        User user = userService.findUserById(id);
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setLogin(user.getLogin());
        profileDTO.setAvatar(user.getAvatar());
        profileDTO.setRegistrationDate(user.getCreated());
        profileDTO.setFollowers(userService.countFollowers(id));
        profileDTO.setLikes(likeService.findByUserId(id).size());
        profileDTO.setIdeas(userService.countAllIdeas(id));
        return ResponseEntity.ok(profileDTO);
    }

    @GetMapping("/followers")
    public  PagedListHolder<UserListDTO> getFollowers(@RequestParam("userId") Long userId,
                                          @RequestParam("keyword") String keyword,
                                          @RequestParam("pageNumber") int pageNumber,
                                          @RequestParam("size") int size) {
        List<User> followers = userService.findAllSubscribers(userId, keyword);
        PagedListHolder<UserListDTO> result = userService.getAdditionalInfoAuthorized(followers, userId, pageNumber, size);
        return result;
    }

    @GetMapping("/followings")
    public PagedListHolder<UserListDTO> getFollowings(@RequestParam("userId") Long userId,
                                           @RequestParam("keyword") String keyword,
                                           @RequestParam("pageNumber") int pageNumber,
                                           @RequestParam("size") int size) {
        List<User> followings = userService.findAllSubscriptions(userId, keyword);
        PagedListHolder<UserListDTO> result = userService.getAdditionalInfoAuthorized(followings, userId, pageNumber, size);
        return result;
    }

    @GetMapping("/search")
    public ResponseEntity<UserDTO> getUserByLogin(@RequestParam String login) {
        User user = userService.findByLogin(login);
        UserDTO userDTO = new UserDTO(user);
        return ResponseEntity.ok(userDTO);
    }
}
