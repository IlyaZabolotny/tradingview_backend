package tradingview.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/markets")
public class MarketController {

    private String base = "https://financialmodelingprep.com/api/v3/";

    private String apiKey = "41ad9d6d4bfbf19bfd4c125de1f4ad09";

    @GetMapping("/actives")
    public List getActives() {
        RestTemplate restTemplate = new RestTemplate();
        List actives = restTemplate.getForObject(
                base+"actives?apikey=" + apiKey,
                List.class
        );
        return actives;
    }

    @GetMapping("/gainers")
    public List getGainers() {
        RestTemplate restTemplate = new RestTemplate();
        List gainers = restTemplate.getForObject(
                base+"gainers?apikey=" + apiKey,
                List.class
        );
        return gainers;
    }

    @GetMapping("/losers")
    public List getLosers() {
        RestTemplate restTemplate = new RestTemplate();
        List losers = restTemplate.getForObject(
                base+"losers?apikey=" + apiKey,
                List.class
        );
        return losers;
    }
}