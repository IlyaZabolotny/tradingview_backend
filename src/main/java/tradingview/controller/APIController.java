package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import tradingview.dto.SearchStockDTO;
import tradingview.dto.StockDTO;
import tradingview.model.FavoriteStock;
import tradingview.service.StockService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/stocks")
public class APIController {

    private String base = "https://financialmodelingprep.com/api/v3/";

    private String apiKey = "41ad9d6d4bfbf19bfd4c125de1f4ad09";

    @Autowired
    private StockService stockService;

    @GetMapping
    public PagedListHolder getStocks(@RequestParam("pageNumber") int pageNumber,
                                     @RequestParam("size") int size,
                                     @RequestParam("keyword") String keyword) {
        RestTemplate restTemplate = new RestTemplate();
        List stocks = restTemplate.getForObject(
                base + "stock-screener?marketCapMoreThan=0&sector=" + keyword + "&limit=100&apikey=" + apiKey,
                List.class
        );
        PagedListHolder stocksPage = new PagedListHolder(stocks);
        stocksPage.setPage(pageNumber);
        stocksPage.setPageSize(size);
        return stocksPage;
    }

    @GetMapping("/{symbol}")
    public StockDTO getStock(@PathVariable String symbol) {
        RestTemplate restTemplate = new RestTemplate();

        ArrayList<HashMap> companyInfo = restTemplate.getForObject(
                getStockUrl("profile", symbol),
                ArrayList.class
        );
        ArrayList<HashMap> quoteInfo = restTemplate.getForObject(
                getStockUrl("quote", symbol),
                ArrayList.class
        );

        HashMap companyValues = companyInfo.get(0);
        HashMap quoteValues = quoteInfo.get(0);

        StockDTO stockDTO = new StockDTO();
        stockDTO.setSymbol((String) quoteValues.get("symbol"));
        stockDTO.setName((String) quoteValues.get("name"));
        stockDTO.setPrice((Double) quoteValues.get("price"));
        stockDTO.setChange((Double) quoteValues.get("change"));
        stockDTO.setChangePercent((Double) quoteValues.get("changesPercentage"));
        stockDTO.setVolume((Integer) quoteValues.get("volume"));
        stockDTO.setMarketCapitalization((Double) quoteValues.get("marketCap"));
        stockDTO.setPriceEarningsRatio((Double) quoteValues.get("pe"));
        stockDTO.setDividendYield((Double) companyValues.get("lastDiv"));
        stockDTO.setEarningsPerShare((Double) quoteValues.get("eps"));
        stockDTO.setFullTimeEmployees((String) companyValues.get("fullTimeEmployees"));
        stockDTO.setEarningsAnnouncement((String) quoteValues.get("earningsAnnouncement"));
        stockDTO.setDescription((String) companyValues.get("description"));
        stockDTO.setExchange((String) companyValues.get("exchangeShortName"));
        stockDTO.setCompanyLogo((String) companyValues.get("image"));

        return stockDTO;
    }

    @GetMapping("/time-series/{symbol}")
    public List getStockTimeSeries(@PathVariable String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        Date date = new Date();
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        long endTime = c.getTimeInMillis()/1000;
        c.add(Calendar.MONTH, -1);
        long startTime = c.getTimeInMillis()/1000;
        HashMap timeSeries = restTemplate.getForObject(
                "https://finnhub.io/api/v1/stock/candle?symbol=" + symbol + "&resolution=1&from=" + startTime + "&to=" + endTime + "&token=c17pkuv48v6sj55b2uf0",
                HashMap.class
        );
        List openList = (List) timeSeries.get("o");
        List highList = (List) timeSeries.get("h");
        List lowList = (List) timeSeries.get("l");
        List closeList = (List) timeSeries.get("c");
        List<Long> timeList = (List<Long>) timeSeries.get("t");
        List result = new ArrayList();
        for (int i = 0; i < openList.size(); i++) {
            List tempList = new ArrayList();
            tempList.add(Long.parseLong(String.valueOf(timeList.get(i)))*1000);
            tempList.add(openList.get(i));
            tempList.add(highList.get(i));
            tempList.add(lowList.get(i));
            tempList.add(closeList.get(i));
            result.add(tempList);
        }
        return result;
    }

    @GetMapping("/time-series-volume/{symbol}")
    public List getStockVolumeTimeSeries(@PathVariable String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        Date date = new Date();
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        long endTime = c.getTimeInMillis()/1000;
        c.add(Calendar.MONTH, -1);
        long startTime = c.getTimeInMillis()/1000;
        HashMap timeSeries = restTemplate.getForObject(
                "https://finnhub.io/api/v1/stock/candle?symbol=" + symbol + "&resolution=1&from=" + startTime + "&to=" + endTime + "&token=c17pkuv48v6sj55b2uf0",
                HashMap.class
        );
        List volumeList = (List) timeSeries.get("v");
        List timeList = (List) timeSeries.get("t");
        List result = new ArrayList();
        for (int i = 0; i < volumeList.size(); i++) {
            List tempList = new ArrayList();
            tempList.add( Long.parseLong(String.valueOf(timeList.get(i)))*1000);
            tempList.add(volumeList.get(i));
            result.add(tempList);
        }
        return result;
    }

    @GetMapping("/favorite/{userId}")
    public List<StockDTO> getFavoriteStocksInfo(@PathVariable("userId") Long userId) {
        List<FavoriteStock> favoriteSymbolsList = stockService.findByUserId(userId);
        RestTemplate restTemplate = new RestTemplate();
        List<StockDTO> result = new ArrayList<>();
        for (FavoriteStock favoriteSymbol : favoriteSymbolsList) {
            ArrayList<HashMap> quoteInfo = restTemplate.getForObject(
                    getStockUrl("quote", favoriteSymbol.getSymbol()),
                    ArrayList.class
            );

            HashMap quoteValues = quoteInfo.get(0);

            StockDTO stockDTO = new StockDTO();
            stockDTO.setSymbol((String) quoteValues.get("symbol"));
            stockDTO.setName((String) quoteValues.get("name"));
            stockDTO.setPrice((Double) quoteValues.get("price"));
            stockDTO.setChange((Double) quoteValues.get("change"));
            stockDTO.setChangePercent((Double) quoteValues.get("changesPercentage"));
            stockDTO.setVolume((Integer) quoteValues.get("volume"));
            stockDTO.setMarketCapitalization((Double) quoteValues.get("marketCap"));
            stockDTO.setPriceEarningsRatio((Double) quoteValues.get("pe"));
            stockDTO.setEarningsPerShare((Double) quoteValues.get("eps"));
            stockDTO.setEarningsAnnouncement((String) quoteValues.get("earningsAnnouncement"));
            stockDTO.setExchange((String) quoteValues.get("exchange"));
            result.add(stockDTO);
        }
        return result;
    }

    @GetMapping("/time-series-full/{symbol}")
    public List getStockTimeSeriesFull(@PathVariable String symbol) {
        RestTemplate restTemplate = new RestTemplate();
        HashMap timeSeriesList = restTemplate.getForObject(
                base + "historical-price-full/" + symbol + "?apikey=" + apiKey,
                HashMap.class
        );
        List historical = (List) timeSeriesList.get("historical");
        List timeSeriesChartList = new ArrayList();
        for (int i = 0; i < historical.size(); i++) {
            HashMap timeSeries = (HashMap) historical.get(i);
            List list = new ArrayList();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = f.parse((String) timeSeries.get("date"));
                long milliseconds = date.getTime();
                list.add(milliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            list.add(timeSeries.get("open"));
            list.add(timeSeries.get("high"));
            list.add(timeSeries.get("low"));
            list.add(timeSeries.get("close"));
            timeSeriesChartList.add(list);
        }
        return timeSeriesChartList;
    }

    @GetMapping("/search")
    public List getStockByTicker(@RequestParam String keyword) {
        RestTemplate restTemplate = new RestTemplate();
        List<HashMap> stocks = restTemplate.getForObject(
                searchStocks(keyword),
                List.class
        );
        List<SearchStockDTO> result = new ArrayList<>();
        for (HashMap stock: stocks) {
            SearchStockDTO searchStockDTO = new SearchStockDTO();
            searchStockDTO.setValue((String) stock.get("symbol"));
            result.add(searchStockDTO);
        }
        return result;
    }

    private String getStockUrl(String function, String symbol) {
        return base + function + "/" + symbol + "?apikey=" + apiKey;
    }

    private String searchStocks(String keyword) {
        return base + "/search-ticker?query=" + keyword + "&limit=10&apikey=" + apiKey;
    }
}