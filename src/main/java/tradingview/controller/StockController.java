package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tradingview.model.FavoriteStock;
import tradingview.service.StockService;

import java.util.List;

@RestController
@RequestMapping("/favorite-stocks")
public class StockController {

   @Autowired
   private StockService stockService;

    @GetMapping
    public List<FavoriteStock> findAllFavoriteStocks() {
        return stockService.findAllStocks();
    }

    @GetMapping("/{userId}")
    public List<FavoriteStock> findFavoriteStocksByUserId(@PathVariable("userId") Long userId) {
        return stockService.findByUserId(userId);
    }

    @GetMapping("/searchByUserIdAndSymbol")
    public boolean findByStockIdAndSymbol(@RequestParam("userId") Long userId, @RequestParam("symbol") String symbol) {
        return stockService.checkByUserIdAndSymbol(userId, symbol);
    }

    @PostMapping("/add")
    public void addFavoriteStock(@RequestBody FavoriteStock favoriteStock) {
        stockService.addStock(favoriteStock);
    }

    @DeleteMapping("/delete")
    public void deleteFavoriteStock(@RequestBody FavoriteStock favoriteStock) {
        stockService.deleteStock(favoriteStock);
    }
}
