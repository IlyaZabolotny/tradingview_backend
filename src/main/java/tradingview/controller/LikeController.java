package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tradingview.model.Like;
import tradingview.service.LikeService;

@RestController
@RequestMapping("/likes")
public class LikeController {

    @Autowired
    private LikeService likeService;

    @PostMapping("/add")
    public void setLike(@RequestBody Like like) {
        likeService.addLike(like);
    }

    @DeleteMapping("/delete")
    public void deleteLike(@RequestBody Like like) {
        likeService.deleteLike(like);
    }
}
