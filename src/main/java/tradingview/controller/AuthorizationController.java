package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import tradingview.dto.LoginDTO;
import tradingview.dto.RegisterDTO;
import tradingview.dto.UserDTO;
import tradingview.model.User;
import tradingview.security.jwt.JwtTokenProvider;
import tradingview.service.UserService;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/authorization")
public class AuthorizationController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;

    @Autowired
    public AuthorizationController(AuthenticationManager authenticationManager,
                                   JwtTokenProvider jwtTokenProvider,
                                   UserService userService
    ) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody RegisterDTO registerUser) {
        if (userService.findByLogin(registerUser.getLogin()) == null){
            User user = new User();
            user.setLogin(registerUser.getLogin());
            user.setEmail(registerUser.getEmail());
            user.setPassword(registerUser.getPassword());
            userService.addUser(user);
            return ResponseEntity.ok(user);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<UserDTO> loginUser(@RequestBody LoginDTO loginDto) {
        try {
            String login = loginDto.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, loginDto.getPassword()));
            User user = userService.findByLoginAndPassword(loginDto);
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + login + " not found");
            }
            user.setAuthorized(true);
            user.setLastSeen(null);
            userService.patchUser(user);
            String token = jwtTokenProvider.createToken(login, user.getRoles());
            Map<Object, Object> response = new HashMap<>();
            UserDTO userDTO = new UserDTO(user);
            userDTO.setToken(token);
            return ResponseEntity.ok(userDTO);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("/logout/{id}")
    public void logoutUser(@PathVariable long id) {
        User user = userService.findUserById(id);
        user.setAuthorized(false);
        user.setLastSeen(new Date());
        userService.patchUser(user);
    }
}