package tradingview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tradingview.model.FavoriteIdea;
import tradingview.service.FavoriteIdeaService;

@RestController
@RequestMapping("/favorite-ideas")
public class FavoriteIdeaController {

    @Autowired
    private FavoriteIdeaService favoriteIdeaService;

    @PostMapping("/add")
    public void setFavoriteIdea(@RequestBody FavoriteIdea favoriteIdea) {
        favoriteIdeaService.addFavoriteIdea(favoriteIdea);
    }

    @DeleteMapping("/delete")
    public void deleteFavoriteIdea(@RequestBody FavoriteIdea favoriteIdea) {
        favoriteIdeaService.deleteFavoriteIdea(favoriteIdea);
    }
}
