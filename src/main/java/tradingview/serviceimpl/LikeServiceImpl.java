package tradingview.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingview.model.Like;
import tradingview.repository.LikeRepository;
import tradingview.service.LikeService;

import java.util.List;
import java.util.Optional;

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeRepository likeRepository;

    @Override
    public List<Like> findAllLikes() {
        return likeRepository.findAll();
    }

    @Override
    public Like findLikeById(Long id) {
        return likeRepository.findById(id).orElse(null);
    }

    @Override
    public void addLike(Like like) {
        likeRepository.save(like);
    }

    @Override
    public void deleteLike(Like like) {
        Optional<Like> tempLike = likeRepository.findLikeByUserIdAndIdeaId(
                like.getUserId(),
                like.getIdeaId());
        if(!tempLike.isPresent())
            return;
        likeRepository.deleteById(tempLike.get().getId());
    }

    @Override
    public Like findByUserIdAndIdeaId(Long userId, Long ideaId) {
        return likeRepository.findLikeByUserIdAndIdeaId(userId, ideaId).orElse(null);
    }

    @Override
    public List<Like> findByIdeaId(Long ideaId) {
        return likeRepository.findLikesByIdeaId(ideaId);
    }

    @Override
    public List<Like> findByUserId(Long userId) {
        return likeRepository.findLikesByUserId(userId);
    }

    @Override
    public Integer countLikesByIdeaId(Long ideaId) {
        return likeRepository.countLikesByIdeaId(ideaId);
    }

    @Override
    public Integer countLikesByIdeaIdAndUserId(Long userId, Long ideaId) {
        return likeRepository.countLikesByIdeaIdAndUserId(ideaId, userId);
    }
}
