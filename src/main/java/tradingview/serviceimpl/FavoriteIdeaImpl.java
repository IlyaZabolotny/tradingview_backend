package tradingview.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingview.model.FavoriteIdea;
import tradingview.model.Like;
import tradingview.repository.FavoriteIdeaRepository;
import tradingview.service.FavoriteIdeaService;

import java.util.List;
import java.util.Optional;

@Service
public class FavoriteIdeaImpl implements FavoriteIdeaService {

    @Autowired
    private FavoriteIdeaRepository favoriteIdeaRepository;

    @Override
    public List<FavoriteIdea> findAllFavoriteIdeas() {
        return favoriteIdeaRepository.findAll();
    }

    @Override
    public FavoriteIdea findFavoriteIdeaById(Long id) {
        return favoriteIdeaRepository.findById(id).orElse(null);
    }

    @Override
    public void addFavoriteIdea(FavoriteIdea favoriteIdea) {
        favoriteIdeaRepository.save(favoriteIdea);
    }

    @Override
    public void deleteFavoriteIdea(FavoriteIdea favoriteIdea) {
        Optional<FavoriteIdea> tempFavoriteIdea = favoriteIdeaRepository.findFavoriteIdeaByUserIdAndIdeaId(
                favoriteIdea.getUserId(),
                favoriteIdea.getIdeaId());
        if(!tempFavoriteIdea.isPresent())
            return;
        favoriteIdeaRepository.deleteById(tempFavoriteIdea.get().getId());
    }

    @Override
    public FavoriteIdea findByUserIdAndIdeaId(Long userId, Long ideaId) {
        return favoriteIdeaRepository.findFavoriteIdeaByUserIdAndIdeaId(userId, ideaId).orElse(null);
    }

    @Override
    public List<FavoriteIdea> findByUserId(Long userId) {
        return favoriteIdeaRepository.findFavoriteIdeasByUserId(userId);
    }

    @Override
    public Integer countFavoriteIdeasByIdeaIdAndUserId(Long userId, Long ideaId) {
        return favoriteIdeaRepository.countFavoriteIdeasByIdeaIdAndUserId(ideaId, userId);
    }
}
