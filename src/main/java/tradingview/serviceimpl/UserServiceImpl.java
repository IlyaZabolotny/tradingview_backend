package tradingview.serviceimpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tradingview.dto.FollowerDTO;
import tradingview.dto.LoginDTO;
import tradingview.dto.UserListDTO;
import tradingview.model.Status;
import tradingview.model.User;
import tradingview.repository.UserRepository;
import tradingview.service.SubscriptionService;
import tradingview.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public List<User> findAllUsers(String keyword) {
        return userRepository.findAllUsers(keyword);
    }

    public User addUser(User user) {
        user.setStatus(Status.ACTIVE);
        user.setAuthorized(false);
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
        user.setPassword(null);
        return user;
    }

    public User findUserById(long id) {
        return userRepository.findById(id).orElse(null);
    }

    public User patchUser(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public User patchPasswordUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login).orElse(null);
    }

    public User findByLoginAndPassword(LoginDTO loginDto) {
        User user = userRepository.findByLogin(loginDto.getLogin()).orElse(null);
        if (user == null) {
            return null;
        }
        if (encoder.matches(loginDto.getPassword(), user.getPassword())) {
            return user;
        }
        return null;
    }

    public List<User> findAllSubscriptions(long id, String keyword) {
        return userRepository.findAllSubscriptions(id, keyword);
    }

    public List<User> findAllSubscribers(long id, String keyword) {
        return userRepository.findAllSubscribers(id, keyword);
    }

    public Long countFollowers(Long idUser) {
        return userRepository.countFollowers(idUser);
    }

    public Long getAllLikes(Long idUser) {
        return userRepository.getAllLikes(idUser);
    }

    public Long countAllIdeas(Long idUser) {
        return userRepository.countAllIdeas(idUser);
    }

    public PagedListHolder<FollowerDTO> getAdditionalInfo(List<User> userList, int pageNumber, int size) {
        Pageable pageable = PageRequest.of(pageNumber, size, Sort.unsorted());
        List<FollowerDTO> result = new ArrayList<>();
        for (User u: userList) {
            result.add(new FollowerDTO(
                    u.getId(),
                    u.getLogin(),
                    u.getStatus(),
                    countFollowers(u.getId()),
                    countAllIdeas(u.getId()),
                    u.getLastSeen()));
        }
        //Page<FollowerDTO> resultPage = new PageImpl<>(result, pageable, size);
        PagedListHolder resultPage = new PagedListHolder(result);
        resultPage.setPageSize(size);
        resultPage.setPage(pageNumber);
        return resultPage;
    }

    public PagedListHolder<UserListDTO> getAdditionalInfoAuthorized(List<User> userList, Long userId, int pageNumber, int size) {
        Pageable pageable = PageRequest.of(pageNumber, size, Sort.unsorted());
        List<UserListDTO> result = new ArrayList<>();
        for (User u: userList) {
            result.add(new UserListDTO(
                    u.getId(),
                    u.getLogin(),
                    u.getStatus(),
                    countFollowers(u.getId()),
                    countAllIdeas(u.getId()),
                    findAllSubscribers(u.getId(), "").contains(findUserById(userId)),// subscriptionService.checkByUserIdAndSubscriberId(u.getId(), userId),
                    u.getLastSeen(),
                    u.getAvatar()
                    ));
        }
        PagedListHolder resultPage = new PagedListHolder(result);
        resultPage.setPageSize(size);
        resultPage.setPage(pageNumber);
        return resultPage;
    }
}