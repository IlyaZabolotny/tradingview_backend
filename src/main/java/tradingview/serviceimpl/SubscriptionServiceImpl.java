package tradingview.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingview.model.Subscription;
import tradingview.repository.SubscriptionRepository;
import tradingview.service.SubscriptionService;

import java.util.Optional;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public void addSubscription(Subscription subscription) {
        subscriptionRepository.save(subscription);
    }

    @Override
    public void deleteSubscription(Subscription subscription) {
        Optional<Subscription> tempSubscription = subscriptionRepository.findSubscriptionByUserIdAndSubscriberId(
                subscription.getUserId(),
                subscription.getSubscribedTo());
        if(!tempSubscription.isPresent())
            return;
        subscriptionRepository.deleteById(tempSubscription.get().getId());
    }

    @Override
    public Subscription findByUserIdAndSubscriberId(Long userId, Long subscriberId) {
        return subscriptionRepository.findSubscriptionByUserIdAndSubscriberId(userId, subscriberId).orElse(null);
    }

    @Override
    public Integer checkByUserIdAndSubscriberId(Long userId, Long subscriberId) {
        return subscriptionRepository.checkSubscriptionByUserIdAndSubscriberId(userId, subscriberId);
    }
}
