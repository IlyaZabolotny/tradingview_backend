package tradingview.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tradingview.model.FavoriteStock;
import tradingview.repository.StockRepository;
import tradingview.service.StockService;

import java.util.List;
import java.util.Optional;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    StockRepository stockRepository;

    @Override
    public List<FavoriteStock> findAllStocks() {
        return stockRepository.findAll();
    }

    @Override
    public FavoriteStock findStockById(Long id) {
        return stockRepository.findById(id).orElse(null);
    }

    @Override
    public void addStock(FavoriteStock favoriteStock) {
        stockRepository.save(favoriteStock);
    }

    @Override
    public void deleteStock(FavoriteStock favoriteStock) {
        Optional<FavoriteStock> tempStock = stockRepository.findStockByUserIdAndSymbol(
                favoriteStock.getUserId(),
                favoriteStock.getSymbol());
        if(!tempStock.isPresent())
            return;
        stockRepository.deleteById(tempStock.get().getId());
    }

    @Override
    public FavoriteStock findByUserIdAndSymbol(Long userId, String symbol) {
        return stockRepository.findStockByUserIdAndSymbol(userId, symbol).orElse(null);
    }

    @Override
    public List<FavoriteStock> findByUserId(Long userId) {
        return stockRepository.findStocksByUserId(userId);
    }

    @Override
    public boolean checkByUserIdAndSymbol(Long userId, String symbol) {
        Optional<FavoriteStock> favoriteStock = stockRepository.findStockByUserIdAndSymbol(userId, symbol);
        if (favoriteStock.isPresent()) {
            return true;
        } else {
            return false;
        }
    }
}
