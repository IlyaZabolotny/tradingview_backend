package tradingview.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import tradingview.dto.FullIdeaDTO;
import tradingview.dto.IdeaDTO;
import tradingview.dto.IdeaListDTO;
import tradingview.model.Idea;
import tradingview.repository.IdeaRepository;
import tradingview.service.FavoriteIdeaService;
import tradingview.service.IdeaService;
import tradingview.service.LikeService;

import java.util.ArrayList;
import java.util.List;

@Service
public class IdeaServiceImpl implements IdeaService {

    @Autowired
    private IdeaRepository ideaRepository;

    @Autowired
    private LikeService likeService;

    @Autowired
    private FavoriteIdeaService favoriteIdeaService;

    public PagedListHolder<FullIdeaDTO> findIdeasByTitle(Long userId, String keyword, int pageNumber, int size) {
        List<IdeaListDTO> ideaListDTOS = ideaRepository.findIdeasByTitle(keyword);
        List<FullIdeaDTO> result = new ArrayList<>();
        for (IdeaListDTO idea: ideaListDTOS) {
            FullIdeaDTO fullIdeaDTO = new FullIdeaDTO();
            fullIdeaDTO.setId(idea.getId());
            fullIdeaDTO.setLogin(idea.getLogin());
            fullIdeaDTO.setAvatar(idea.getAvatar());
            fullIdeaDTO.setCreated(idea.getCreated());
            fullIdeaDTO.setImage(idea.getImage());
            fullIdeaDTO.setTitle(idea.getTitle());
            fullIdeaDTO.setContent(idea.getContent());
            fullIdeaDTO.setLikesCount(likeService.countLikesByIdeaId(idea.getId()));
            fullIdeaDTO.setLiked(likeService.countLikesByIdeaIdAndUserId(userId, idea.getId()));
            fullIdeaDTO.setAddedToFav(favoriteIdeaService.countFavoriteIdeasByIdeaIdAndUserId(userId, idea.getId()));
            result.add(fullIdeaDTO);
        }
        PagedListHolder resultPage = new PagedListHolder(result);
        resultPage.setPageSize(size);
        resultPage.setPage(pageNumber);
        MutableSortDefinition x = new MutableSortDefinition("created", true, false);
        resultPage.setSort(x);
        return resultPage;
    }

    public PagedListHolder<FullIdeaDTO> findPublishedIdeas(Long userId, String keyword, int pageNumber, int size) {
        List<IdeaListDTO> ideaListDTOS = ideaRepository.getIdeasListSearch(userId, keyword);
        List<FullIdeaDTO> result = new ArrayList<>();
        for (IdeaListDTO idea: ideaListDTOS) {
            FullIdeaDTO fullIdeaDTO = new FullIdeaDTO();
            fullIdeaDTO.setId(idea.getId());
            fullIdeaDTO.setLogin(idea.getLogin());
            fullIdeaDTO.setAvatar(idea.getAvatar());
            fullIdeaDTO.setCreated(idea.getCreated());
            fullIdeaDTO.setImage(idea.getImage());
            fullIdeaDTO.setTitle(idea.getTitle());
            fullIdeaDTO.setContent(idea.getContent());
            fullIdeaDTO.setLikesCount(likeService.countLikesByIdeaId(idea.getId()));
            fullIdeaDTO.setLiked(likeService.countLikesByIdeaIdAndUserId(userId, idea.getId()));
            fullIdeaDTO.setAddedToFav(favoriteIdeaService.countFavoriteIdeasByIdeaIdAndUserId(userId, idea.getId()));
            result.add(fullIdeaDTO);
        }
        PagedListHolder resultPage = new PagedListHolder(result);
        resultPage.setPageSize(size);
        resultPage.setPage(pageNumber);
        return resultPage;
    }

    @Override
    public Page<IdeaListDTO> findIdeasByTitleUnauthorized(String keyword, int pageNumber, int size) {
        Pageable pageable = PageRequest.of(pageNumber, size, Sort.unsorted());
        return ideaRepository.findIdeasByTitleUnauthorized(keyword, pageable);
    }

    public Idea findIdeaById(long id) {
        return ideaRepository.findById(id).orElse(null);
    }

    public Idea addIdea(Idea idea) {
        ideaRepository.save(idea);
        return idea;
    }

    public Idea patchIdea(IdeaDTO ideaDTO) {
        Idea idea = ideaRepository.findById(ideaDTO.getId()).orElse(null);
        idea.setAuthor(ideaDTO.getAuthor());
        idea.setContent(ideaDTO.getContent());
        idea.setImage(ideaDTO.getImage());
        idea.setTitle(ideaDTO.getTitle());
        ideaRepository.save(idea);
        return idea;
    }

    @Override
    public Page<IdeaListDTO> getIdeasList(Long authorId, int pageNumber, int size) {
        return null;
    }

    public Page<IdeaListDTO> findIdeas(int pageNumber, int size) {
        Pageable pageable = PageRequest.of(pageNumber, size, Sort.unsorted());
        return ideaRepository.findIdeas(pageable);
    }

    public List<Idea> findIdeasByAuthor(Long authorId) {
            return ideaRepository.findIdeasByAuthor(authorId);
    }

    public PagedListHolder<FullIdeaDTO> findFavoriteIdeas(long authorId, String keyword, int pageNumber, int size) {
        List<IdeaListDTO> ideaListDTOS =  ideaRepository.findFavoriteIdeas(authorId, keyword);
        List<FullIdeaDTO> result = new ArrayList<>();
        for (IdeaListDTO idea: ideaListDTOS) {
            FullIdeaDTO fullIdeaDTO = new FullIdeaDTO();
            fullIdeaDTO.setId(idea.getId());
            fullIdeaDTO.setLogin(idea.getLogin());
            fullIdeaDTO.setAvatar(idea.getAvatar());
            fullIdeaDTO.setCreated(idea.getCreated());
            fullIdeaDTO.setImage(idea.getImage());
            fullIdeaDTO.setTitle(idea.getTitle());
            fullIdeaDTO.setContent(idea.getContent());
            fullIdeaDTO.setLikesCount(likeService.countLikesByIdeaId(idea.getId()));
            fullIdeaDTO.setLiked(likeService.countLikesByIdeaIdAndUserId(authorId, idea.getId()));
            fullIdeaDTO.setAddedToFav(favoriteIdeaService.countFavoriteIdeasByIdeaIdAndUserId(authorId, idea.getId()));
            result.add(fullIdeaDTO);
        }
        PagedListHolder resultPage = new PagedListHolder(result);
        resultPage.setPageSize(size);
        resultPage.setPage(pageNumber);
        return resultPage;
    }

    public List<Idea> findIdeasByTag(String tag) {
        return ideaRepository.findIdeasByTag(tag);
    }
}
