package tradingview.service;

import tradingview.model.FavoriteIdea;

import java.util.List;

public interface FavoriteIdeaService {

    List<FavoriteIdea> findAllFavoriteIdeas();
    FavoriteIdea findFavoriteIdeaById(Long id);
    void addFavoriteIdea(FavoriteIdea favoriteIdea);
    void deleteFavoriteIdea(FavoriteIdea favoriteIdea);
    FavoriteIdea findByUserIdAndIdeaId(Long userId, Long ideaId);
    List<FavoriteIdea> findByUserId(Long userId);
    Integer countFavoriteIdeasByIdeaIdAndUserId(Long userId, Long ideaId);
}
