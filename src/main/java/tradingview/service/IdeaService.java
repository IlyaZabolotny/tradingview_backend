package tradingview.service;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tradingview.dto.FullIdeaDTO;
import tradingview.dto.IdeaDTO;
import tradingview.dto.IdeaListDTO;
import tradingview.dto.SearchDTO;
import tradingview.model.Idea;
import java.util.List;

public interface IdeaService {

    Page<IdeaListDTO> findIdeas(int pageNumber, int size);

    PagedListHolder<FullIdeaDTO> findIdeasByTitle(Long userId, String keyword, int pageNumber, int size);

    Page<IdeaListDTO> findIdeasByTitleUnauthorized(String keyword, int pageNumber, int size);

    PagedListHolder<FullIdeaDTO> findPublishedIdeas(Long userId, String keyword, int pageNumber, int size);
    
    Idea findIdeaById(long id);
    
    Idea addIdea(Idea idea);
    
    Idea patchIdea(IdeaDTO idea);

    Page<IdeaListDTO> getIdeasList(Long authorId, int pageNumber, int size);

    PagedListHolder<FullIdeaDTO> findFavoriteIdeas(long authorId, String keyword, int pageNumber, int size);

    List<Idea> findIdeasByTag(String tag);
}
