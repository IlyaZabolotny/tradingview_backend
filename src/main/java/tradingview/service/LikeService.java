package tradingview.service;

import org.springframework.data.repository.query.Param;
import tradingview.model.Like;

import java.util.List;

public interface LikeService {

    List<Like> findAllLikes();
    Like findLikeById(Long id);
    void addLike(Like like);
    void deleteLike(Like like);
    Like findByUserIdAndIdeaId(Long userId, Long ideaId);
    List<Like> findByIdeaId(Long ideaId);
    List<Like> findByUserId(Long userId);
    Integer countLikesByIdeaId(Long ideaId);
    Integer countLikesByIdeaIdAndUserId(Long userId, Long ideaId);
}
