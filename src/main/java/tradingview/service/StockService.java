package tradingview.service;

import tradingview.model.FavoriteStock;

import java.util.List;

public interface StockService {

    List<FavoriteStock> findAllStocks();
    FavoriteStock findStockById(Long id);
    void addStock(FavoriteStock favoriteStock);
    void deleteStock(FavoriteStock favoriteStock);
    FavoriteStock findByUserIdAndSymbol(Long userId, String symbol);
    List<FavoriteStock> findByUserId(Long userId);
    boolean checkByUserIdAndSymbol(Long userId, String symbol);
}
