package tradingview.service;

import tradingview.model.Subscription;


public interface SubscriptionService {

    void addSubscription(Subscription subscription);
    void deleteSubscription(Subscription subscription);
    Subscription findByUserIdAndSubscriberId(Long userId, Long subscriberId);
    Integer checkByUserIdAndSubscriberId(Long userId, Long subscriberId);
}
