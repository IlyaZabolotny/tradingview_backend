package tradingview.service;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import tradingview.dto.FollowerDTO;
import tradingview.dto.LoginDTO;
import tradingview.dto.UserListDTO;
import tradingview.model.User;
import java.util.List;

public interface UserService {

    List<User> findAllUsers(String keyword);

    User findUserById(long id);

    User addUser(User user);

    User patchUser(User user);

    User patchPasswordUser(User user);

    User findByLogin(String login);

    User findByLoginAndPassword(LoginDTO loginDto);

    List<User> findAllSubscriptions(long id, String keyword);

    List<User> findAllSubscribers(long id, String keyword);

    Long countFollowers(Long idUser);

    Long getAllLikes(Long idUser);

    Long countAllIdeas(Long idUser);

    PagedListHolder<FollowerDTO> getAdditionalInfo(List<User> userList, int pageNumber, int size);

    PagedListHolder<UserListDTO> getAdditionalInfoAuthorized(List<User> userList, Long userId, int pageNumber, int size);
}