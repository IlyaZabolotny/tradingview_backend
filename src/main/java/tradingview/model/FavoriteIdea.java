package tradingview.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "favorite_ideas")
public class FavoriteIdea {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long ideaId;

    private Long userId;
}