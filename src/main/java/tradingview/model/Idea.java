package tradingview.model;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name="ideas")
public class Idea extends BaseEntity {

    private String title;

    @Column(columnDefinition = "TEXT")
    private String content;

    @Type(type="org.hibernate.type.BinaryType")
    private byte[] image;

    @Column(name="author_id")
    private Long author;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "idea_tag",
            joinColumns = {@JoinColumn(name = "idea_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id", referencedColumnName = "id")})
    private List<Tag> tags;
}