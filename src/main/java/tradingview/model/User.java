package tradingview.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Getter
    @Setter
    private byte[] avatar;

    @Getter
    @Setter
    @Column(name = "firstname")
    private String firstName;

    @Getter
    @Setter
    @Column(name = "lastname")
    private String lastName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String login;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String company;

    @Getter
    @Setter
    @Column(name = "authorized")
    private Boolean Authorized;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private Status status;

    @Getter
    @Setter
    private Date lastSeen;

    @OneToMany(mappedBy = "author")
    private List<Idea> authorIdeas;

//    @ManyToMany
//    @JoinTable(name = "favorite_ideas",
//            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "idea_id", referencedColumnName = "id")})
//    private List<Idea> ideas;
//
//    @ManyToMany(mappedBy = "likes")
//    private List<Idea> likes;

//    @ManyToMany
//    @JoinTable(name = "subscriptions",
//            joinColumns = {@JoinColumn(name = "user_id")},
//            inverseJoinColumns = {@JoinColumn(name = "subscribed_to")})
//    private List<User> users;
//
//    @ManyToMany(mappedBy = "users")
//    private List<User> subscribers;

    @Getter
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;
}