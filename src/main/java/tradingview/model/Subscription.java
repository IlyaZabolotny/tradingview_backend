package tradingview.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "subscriptions")
public class Subscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    @Column(name="subscribed_to")
    private Long subscribedTo;
}
