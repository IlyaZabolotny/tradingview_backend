package tradingview.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import tradingview.security.jwt.JwtConfigurer;
import tradingview.security.jwt.JwtTokenProvider;


@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    private final JwtTokenProvider jwtTokenProvider;

    private static final String LOGIN_ENDPOINT = "/authorization/**";
    private static final String IDEA_ENDPOINT = "/ideas/unauthorized";
    private static final String USER_ENDPOINT = "/users/unauthorized";
    private static final String ALL_STOCKS_ENDPOINT = "/stocks";
    private static final String STOCKS_SEARCH_ENDPOINT = "/stocks/search";
    private static final String  STOCK_BY_SYMBOL_ENDPOINT = "/stocks/{^[\\d]$}";
    private static final String  IDEA_BY_ID_ENDPOINT = "/ideas/{^[\\d]$}";
    private static final String TIME_SERIES_ENDPOINT = "/stocks/time-series/**";
    private static final String TIME_SERIES_VOLUME_ENDPOINT = "/stocks/time-series-volume/**";
    private static final String MARKETS_ENDPOINT = "/markets/**";

    @Autowired
    public WebSecurityConfiguration(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                cors().and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(IDEA_ENDPOINT).permitAll()
                .antMatchers(USER_ENDPOINT).permitAll()
                .antMatchers(ALL_STOCKS_ENDPOINT).permitAll()
                .antMatchers(STOCK_BY_SYMBOL_ENDPOINT).permitAll()
                .antMatchers(TIME_SERIES_ENDPOINT).permitAll()
                .antMatchers(TIME_SERIES_VOLUME_ENDPOINT).permitAll()
                .antMatchers(MARKETS_ENDPOINT).permitAll()
                .antMatchers(STOCKS_SEARCH_ENDPOINT).permitAll()
                .antMatchers(IDEA_BY_ID_ENDPOINT).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }

    @Override
    public void configure(WebSecurity registry) {
        registry.ignoring().antMatchers("/websocket/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")
                .allowedMethods("GET", "POST", "PATCH", "DELETE");
    }
}
