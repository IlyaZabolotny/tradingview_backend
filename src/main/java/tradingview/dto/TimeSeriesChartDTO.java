package tradingview.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TimeSeriesChartDTO {

    long x;

    Double open;

    Double low;

    Double high;

    Double close;

    Integer volume;
}
