package tradingview.dto;

import java.util.Date;

public interface IdeaListDTO {

    Long getId();

    String getTitle();

    String getContent();

    byte[] getImage();

    Date getCreated();

    String getLogin();

    byte[] getAvatar();
}
