package tradingview.dto;

import lombok.Data;
import tradingview.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UserDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private Date registrationDate;

    private String email;

    private String login;

    private String token;

    private List<String> roles;

    private String company;

    public UserDTO(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.registrationDate=user.getCreated();
        this.email = user.getEmail();
        this.login = user.getLogin();
        this.company = user.getCompany();
        roles=new ArrayList<>();
        user.getRoles().forEach(role ->
                roles.add(role.getName()));
    }
}