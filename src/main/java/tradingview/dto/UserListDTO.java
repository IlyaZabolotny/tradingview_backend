package tradingview.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tradingview.model.Status;

import java.util.Date;

@Data
@AllArgsConstructor
public class UserListDTO {

    private Long id;

    private String login;

    private Status status;

    private Long followers;

    private Long ideas;

    private Boolean subscribed;

    private Date lastSeenTime;

    private byte[] avatar;
}
