package tradingview.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ProfileDTO {

    private String login;

    private Date registrationDate;

    private Integer likes;

    private Long ideas;

    private Long followers;

    private byte[] avatar;
}
