package tradingview.dto;

import lombok.Data;

import java.util.Date;

@Data
public class FullIdeaDTO {

    Long id;

    String login;

    byte[] avatar;

    Date created;

    byte[] image;

    String title;

    String content;

    Integer likesCount;

    Integer liked;

    Integer addedToFav;
}
