package tradingview.dto;

import lombok.Data;

@Data
public class IdeaDTO {
    Long id;

    private String title;

    private String content;

    private byte[] image;

    private Long author;
}
