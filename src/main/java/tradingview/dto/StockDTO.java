package tradingview.dto;

import lombok.Data;

@Data
public class StockDTO {

    private String symbol;

    private String name;

    private Double price;

    private Double change;

    private Double changePercent;

    private Integer volume;

    private Double marketCapitalization;

    private Double priceEarningsRatio;

    private Double dividendYield;

    private Double earningsPerShare;

    private String fullTimeEmployees;

    private String earningsAnnouncement;

    private String description;

    private String exchange;

    private String companyLogo;

}
