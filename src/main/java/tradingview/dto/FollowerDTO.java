package tradingview.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tradingview.model.Status;

import java.util.Date;

@Data
@AllArgsConstructor
public class FollowerDTO {

    private long id;

    private String login;

    private Status status;

    private Long followers;

    private Long ideas;

    private Date lastSeenTime;

}
