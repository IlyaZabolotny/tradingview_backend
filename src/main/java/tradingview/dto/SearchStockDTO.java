package tradingview.dto;

import lombok.Data;

@Data
public class SearchStockDTO {

    String value;
}
