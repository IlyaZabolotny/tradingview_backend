package tradingview.dto;

import lombok.Data;
import javax.validation.constraints.*;

@Data
public class RegisterDTO {

    @NotBlank
    private String login;

    @Email
    @NotBlank
    private String email;

    @Size(min = 5 , max = 20)
    private String password;
}