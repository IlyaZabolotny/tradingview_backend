package tradingview.dto;

import lombok.Data;

@Data
public class TimeSeriesDTO {

    String date;

    Double open;

    Double low;

    Double high;

    Double close;

    Double volume;
}
