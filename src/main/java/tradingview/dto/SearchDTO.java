package tradingview.dto;

import lombok.Data;

@Data
public class SearchDTO {

    private String title;

    private int page;

    private int size;
}
