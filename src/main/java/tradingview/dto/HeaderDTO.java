package tradingview.dto;

import lombok.Data;

@Data
public class HeaderDTO {

    String login;

    byte[] avatar;
}
